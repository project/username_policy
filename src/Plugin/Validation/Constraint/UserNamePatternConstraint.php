<?php

namespace Drupal\username_policy\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value is a valid user name.
 *
 * @Constraint(
 *   id = "UserNamePattern",
 *   label = @Translation("User name pattern", context = "Validation"),
 * )
 */
class UserNamePatternConstraint extends Constraint {

  /**
   * Validation message if username begin with a space.
   *
   * @var string
   */
  public $spaceBeginMessage = 'The username cannot begin with a space.';

  /**
   * Validation message if username end with a space.
   *
   * @var string
   */
  public $spaceEndMessage = 'The username cannot end with a space.';

  /**
   * Validation message if username contain multiple spaces.
   *
   * @var string
   */
  public $multipleSpacesMessage = 'The username cannot contain multiple spaces in a row.';

  /**
   * Validation message if username contains an illegal character.
   *
   * @var string
   */
  public $illegalMessage = 'The username contains an illegal character.';

  /**
   * Validation message if username is too long.
   *
   * @var string
   */
  public $tooLongMessage = 'The username %name is too long: it must be %max characters or less.';

}
